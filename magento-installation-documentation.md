# Magento Installation on Linux mint 20.02

## Install PHP, MariaDB, Nginx.
### NGINX Installation:
___
* Installing Nginx and it’s all library
```
sudo apt install nginx-full
```
### Verify Installation:
___
* Check with status command
```
sudo systemctl status nginx
```
* Status should be active (running)
### PHP Installation:
___
* install PHP(8.0), fpm(fastcgi support) and other library

```
sudo apt install php8.0-cli php8.0-fpm php8.0-mbstring php8.0-gd php8.0-xml
php8.0-mysql php8.0-curl php8.0-intl php8.0-openssl php8.0-zip php8.0-soap php8.0-bcmath
```
### Latest Composer Installation:
___
* First, update the package manager cache by running:

``` 
sudo apt update
```
* Next, run the following command to install the required packages:
```
sudo apt install unzip
```
* Always install latest stable release
```
# Downloading the latest version
 php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

# Checking the file
 php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

# Installing globally 
 sudo php composer-setup.php --install-dir=/bin --filename=composer

# removing unnecessary file
 php -r "unlink('composer-setup.php');"

```
* Then add composer vendor files to path
```
# add this line to .bashrc file (make sure file path exist)
  export PATH="$PATH:/home/shanu/.config/composer/vendor/bin"
  
       *** use username in the place of shanu ***
```
### Secure MariaDB Installation:
___
* Install MariaDB
```
sudo apt install mariadb-server mariadb-test  
```
* Start the process
```
sudo mysql_secure_installation
```
* Answer Y for start password validation plugin
* Switch authentication method from auth_socket to mysql_native_password
```
sudo mariadb
```
* then do some changes in the table
```
-- See the table data
SELECT user, authentication_string, plugin, host FROM mysql.user;
-- add new user and password
GRANT ALL ON *.* TO 'admin/root'@'localhost' IDENTIFIED BY 'password' WITH GRANT OPTION;
-- clean default
FLUSH PRIVILEGES;
-- Check the changes 
SELECT user, authentication_string, plugin, host FROM mysql.user;
-- If all ok exit
exit;
```
### CURL Installation:
___
* Installing curl
```
sudo apt install curl
```
___
## Magento Installation using Composer
___
* Magento Open Source

```
composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition <install-directory-name>
```
## Elasticsearch Installation
___
* Import the Elasticsearch PGP Key
```
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
```
* Installing from the APT repository
```
sudo apt-get install apt-transport-https
```
* Save the repository definition to ``` /etc/apt/sources.list.d/elastic-7.x.list ```:
```
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
```
